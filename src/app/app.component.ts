import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="home">home</button>
    <button routerLink="login">login</button>
    <button routerLink="users">users</button>
    <input type="text">
    <hr>
    
    <router-outlet></router-outlet>
    
    <hr>
    <footer>copyright Mr Biondi</footer>
    
    
  `,
})
export class AppComponent {
  page: string;
}

import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation.service';

@Component({
  selector: 'app-login',
  template: `
    <p>
      login works!
    </p>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(operationService: OperationService) {
    console.log(operationService.theme)
  }

  ngOnInit(): void {
  }

}

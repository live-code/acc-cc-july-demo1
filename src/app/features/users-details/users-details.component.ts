import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../../model/user';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users-details',
  template: `
    <div class="alert alert-danger" *ngIf="error">
      <div *ngIf="error.status === 404">User not found</div>
      <div *ngIf="error.status !== 404">Errore generico</div>
    </div>
    
    <div *ngIf="!error">
      <form #f="ngForm" (submit)="save(f)">
        <input type="text" placeholder="user name" [ngModel]="data?.name" name="name" required > 
        <input type="text" placeholder="age" [ngModel]="data?.age" name="age">
        <button type="submit" [disabled]="f.invalid">Save</button>
      </form>
      <h1>name: {{data?.name}}</h1>
      <div>età: {{data?.age}}</div>
    </div>
    
    <button routerLink="../">back to list</button>
  `,
})
export class UsersDetailsComponent {
  data: User;
  error: HttpErrorResponse;
  id: number;

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) {
    activatedRoute.params
      .pipe(
        switchMap(param => http.get<User>('http://localhost:3000/users/' + param.id))
      )
      .subscribe(
        res => this.data = res,
        err => this.error = err
      );


    /*activatedRoute.params
      .subscribe(params => {
        http.get<User>('http://localhost:3000/users/' + params.id)
          .subscribe(
            res => this.data = res,
            err => this.error = err
          );
    });*/


    /*this.id = activatedRoute.snapshot.params.id;
    http.get<User>('http://localhost:3000/users/' + this.id)
      .subscribe(
        res => this.data = res,
        err => this.error = err
      );*/
  }


  save(form: NgForm): void {
    console.log(form.value)

    this.http.patch<User>('http://localhost:3000/users/' + this.id, form.value)
      .subscribe((res) => {
        this.data = res;
      })
  }
}

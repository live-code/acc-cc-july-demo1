import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation.service';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor(operationService: OperationService) {
    console.log(operationService.theme)
    operationService.theme = 'light'
  }

  ngOnInit(): void {
  }

}

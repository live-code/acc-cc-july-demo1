import { Component } from '@angular/core';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users-component',
  template: `
    <div class="container">
      <form #f="ngForm" (submit)="userService.save(f)">

        <small *ngIf="inputName.errors?.required">Required</small>
        <small *ngIf="inputName.errors?.minlength">Too short. Min 3 chars</small>

        <div class="form-group">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text faex">
                <i
                  class="fa"
                  [ngClass]="{
                    'fa-check': inputName.valid, 
                    'fa-exclamation-triangle': inputName.errors?.required, 
                    'fa-sort-numeric-asc': inputName.errors?.minlength 
                  }"
                  [style.color]="inputName.valid ? 'green' : 'red' "
                ></i>
              </div>
            </div>
            <input
              type="text"
              class="form-control"
              placeholder="Name"
              #inputName="ngModel"
              [ngModel]="userService.activeUser?.name"
              name="name"
              required
              minlength="3"
            >
          </div>


        </div>
        <div class="form-group">
          <input
            type="text" class="form-control"
            [ngModel]="userService.activeUser?.age"
            name="age" required placeholder="Age"
            #inputAge="ngModel"
            [ngClass]="{'is-invalid': inputAge.invalid && f.dirty}"
          >
        </div>
        <button type="submit" class="btn btn-primary" [disabled]="f.invalid">
          {{userService.activeUser ? 'EDIT' : 'ADD'}}
        </button>

        <button type="button" (click)="userService.clear(f)">CLEAR</button>
      </form>

      <hr>

      <div
        *ngFor="let user of userService.users"
        (click)="userService.setActiveUser(user)"
        class="list-group-item"
        [ngClass]="{
          'female': user.id === userService.activeUser?.id && user.gender === 'F',
          'male': user.id === userService.activeUser?.id && (user.gender === 'M' || !user.gender)
        }"
      >

        <app-icon [gender]="user.gender"></app-icon>

        {{user.name}} ( {{user.id }} )

        <span *ngIf="user.age">- <strong>Age: {{user.age}}</strong></span>

        <div class="pull-right">
          <i class="fa fa-info-circle" [routerLink]="'/users/' + user.id"></i>
          <i class="fa fa-trash" 
             (click)="userService.deleteHandler(user.id, $event)"></i>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .female { background: deeppink }  
    .male { background: cornflowerblue }  
  `]
})
export class UsersComponent {
  constructor(public userService: UsersService) {
    userService.getAll();
  }
}

import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: User[];
  activeUser: User;

  constructor(private http: HttpClient) {}

  getAll(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => this.users = res)
  }

  deleteHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();

    this.http.delete('http://localhost:3000/users/' + id)
      .subscribe(() => {

        this.users = this.users.filter(u => u.id !== id);
        if (id === this.activeUser?.id) {
          this.activeUser = null;
        }
      });
  }

  save(form: NgForm): void {
    if (this.activeUser) {
      this.edit(form);
    } else {
      this.add(form);
    }
  }

  edit(form: NgForm): void {
    this.http.patch<User>('http://localhost:3000/users/' + this.activeUser.id, form.value)
      .subscribe(res => {
        this.users = this.users.map(u => {
          if (u.id === this.activeUser.id) {
            return {...u, ...form.value};
          }
          return u;
        });
      });
  }

  add(form: NgForm): void {
    const newUser = {...form.value, gender: 'M'}
    this.http.post<User>('http://localhost:3000/users/', newUser)
      .subscribe(res => {
        this.users = [...this.users, res]
        form.reset();
        this.activeUser = newUser;
      });
  }


  setActiveUser(user: User): void {
    console.log('set active')
    this.activeUser = user;
  }


  clear(form: NgForm): void {
    form.reset();
    this.activeUser = null;
  }
}

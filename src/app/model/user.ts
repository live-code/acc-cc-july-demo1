export interface User {
  id: number;
  name: string;
  age?: number;
  birthday?: number;
  city?: string;
  gender?: string;
}

export interface CSSProps {
  fontSize?: string;
  backgroundColor?: string;
}


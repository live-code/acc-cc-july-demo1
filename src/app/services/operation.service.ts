import { Injectable } from '@angular/core';

@Injectable()
export class OperationService {
  theme = 'dark';

  add(a, b): number {
    return a + b;
  }
}


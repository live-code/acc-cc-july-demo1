import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HelloComponent } from './components/hello.component';
import { IconComponent } from './components/icon.component';
import { UsersComponent } from './features/users/users.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { RouterModule } from '@angular/router';
import { UsersDetailsComponent } from './features/users-details/users-details.component';
import { OperationService } from './services/operation.service';
import { UsersService } from './features/users/services/users.service';

@NgModule({
  declarations: [
    AppComponent, HelloComponent, IconComponent, UsersComponent, HomeComponent, LoginComponent, UsersDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'users', component: UsersComponent  },
      { path: 'users/:id', component: UsersDetailsComponent  },
      { path: 'login', component: LoginComponent },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: '' },
    ])
  ],
  providers: [
    OperationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, Input, OnInit } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'app-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-mars': gender === 'M',
        'fa-venus': gender === 'F'
      }"
    ></i>
  `,
})
export class IconComponent {
  @Input() gender: string;
}

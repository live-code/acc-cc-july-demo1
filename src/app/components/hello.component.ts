import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    hello {{label}}! 
  `
})
export class HelloComponent {
  @Input() label = 'Fabio';
}
